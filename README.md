# MyBatis ExtJS Demo

A simple application with CRUD operations in one grid
___ 

**Tools:**

* Spring
* MyBatis
* ExtJS 6
* Postgres
___ 

Demo application is hosted at [Heroku](https://extjs-mybatis-petshop.herokuapp.com/ "Demobase")
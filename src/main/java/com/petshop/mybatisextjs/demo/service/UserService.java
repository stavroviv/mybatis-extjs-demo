package com.petshop.mybatisextjs.demo.service;

import com.petshop.mybatisextjs.demo.model.User;
import com.petshop.mybatisextjs.demo.mybatis.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public List<User> getAllUsers() {
        return userMapper.getAllUsers();
    }

    @Transactional
    public int createOrUpdate(User user) {
        return user.getId() == 0 ? userMapper.create(user) : userMapper.update(user);
    }

    @Transactional
    public boolean delete(int userId) {
        return userMapper.deleteUserById(userId);
    }
}
package com.petshop.mybatisextjs.demo.mybatis;

import com.petshop.mybatisextjs.demo.model.User;

import java.util.List;

public interface UserMapper {

    List<User> getAllUsers();

    boolean deleteUserById(int userId);

    int create(User user);

    int update(User user);

}

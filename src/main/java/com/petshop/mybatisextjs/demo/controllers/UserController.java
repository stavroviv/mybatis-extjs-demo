package com.petshop.mybatisextjs.demo.controllers;

import com.petshop.mybatisextjs.demo.model.User;
import com.petshop.mybatisextjs.demo.service.UserService;
import org.cloudinary.json.JSONArray;
import org.cloudinary.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller(value = "user-api")
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }

    @GetMapping(produces = "application/json; charset=utf-8")
    @ResponseBody
    public String view() {
        JSONArray results = new JSONArray();
        userService.getAllUsers().forEach(contact -> {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("id", contact.getId());
            jsonObj.put("first", contact.getFirst());
            jsonObj.put("last", contact.getLast());
            jsonObj.put("email", contact.getEmail());
            jsonObj.put("phone", contact.getPhone());
            results.put(jsonObj);
        });
        return getJsonResult(results);
    }

    @PostMapping
    @ResponseBody
    public String getNewId() {
        JSONArray results = new JSONArray();
        JSONObject jsonObj = new JSONObject();
        User newUser = new User();
        userService.createOrUpdate(newUser);
        jsonObj.put("id", newUser.getId());
        results.put(jsonObj);
        return getJsonResult(results);
    }

    @DeleteMapping("/user-api/{id}")
    @ResponseBody
    public boolean delete(@PathVariable String id) {
        try {
            userService.delete(Integer.parseInt(id));
        } catch (NumberFormatException e) {
        }
        return true;
    }

    @PutMapping
    @ResponseBody
    public boolean update(@RequestBody User user) {
        userService.createOrUpdate(user);
        return true;
    }

    private String getJsonResult(JSONArray data) {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("total", data.length());
        jsonObj.put("data", data);
        jsonObj.put("success", true);
        return jsonObj.toString();
    }
}

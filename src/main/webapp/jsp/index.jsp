<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>MyBatis ExtJS Demo</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png" type="image/png"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/extjs/6.0.0/ext-all-debug.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/extjs/6.0.0/ext-all.js"></script>

    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/extjs/6.0.0/classic/theme-triton/resources/theme-triton-all-debug.css">
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/extjs/6.0.0/classic/theme-triton/theme-triton-debug.js"></script>

    <script type="text/javascript" src="app/app.js"></script>

    <style type="text/css">
        #users-grid .x-grid-with-row-lines .x-grid-item.x-grid-item-selected {
            BACKGROUND-COLOR: #5fa2dd
        }
    </style>

</head>
<body></body>
</html>

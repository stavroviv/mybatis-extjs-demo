Ext.require(['Ext.data.*', 'Ext.grid.*']);

Ext.define('User', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, 'email', 'first', 'last', 'phone']
});

Ext.onReady(function () {

    var currentRow = 0;

    var store = Ext.create('Ext.data.Store', {
        autoLoad: true,
        autoSync: true,
        model: 'User',
        proxy: {
            type: 'rest',
            url: '/user-api',
            reader: {
                type: 'json',
                rootProperty: 'data'
            },
            writer: {
                type: "json",
                writeAllFields: true
            }
        },
        listeners: {
            write: function (store, operation) {
                var record = operation.getRecords()[0],
                    name = Ext.String.capitalize(operation.action),
                    verb;
                if (name === 'Destroy') {
                    verb = 'Deleted';
                } else {
                    verb = name + 'd';
                }
                Ext.toast({
                    html: Ext.String.format("{0} user: {1}", verb, record.getId()),
                    width: 200,
                    align: 'br'
                });
            }
        }
    });

    var grid = Ext.create('Ext.grid.Panel', {
        renderTo: document.body,
        plugins: {
            ptype: 'rowediting',
            clicksToEdit: 2,
        },
        width: '100%',
        height: '100%',
        title: 'Users',
        store: store,
        id: 'users-grid',
        columns: [{
            text: 'ID',
            width: 50,
            menuDisabled : true,
            // sortableColumn: false,
            dataIndex: 'id',
            renderer: function (v, meta, rec) {
                return rec.phantom ? '' : v;
            }
        }, {
            text: 'Email',
            width: 200,
            dataIndex: 'email',
            field: {
                xtype: 'textfield'
                // vtype: 'email'
            }
        }, {
            header: 'Phone',
            width: 150,
            dataIndex: 'phone',
            field: {
                xtype: 'textfield'
            },
            renderer: function (v) {
                return Ext.String.htmlEncode(v);
            }
        }, {
            header: 'First name',
            width: 150,
            dataIndex: 'first',
            field: {
                xtype: 'textfield'
            },
            renderer: function (v) {
                return Ext.String.htmlEncode(v);
            }
        }, {
            text: 'Last name',
            width: 150,
            flex: 1,
            dataIndex: 'last',
            field: {
                xtype: 'textfield'
            },
            renderer: function (v) {
                return Ext.String.htmlEncode(v);
            }
        }],
        // restore current row
        listeners: {
            render: function (grid) {
                grid.store.on('load', function (store, records, options) {
                    grid.getSelectionModel().select(currentRow === 0 ? 0 : store.getById(currentRow));
                });
            }
        },
        dockedItems: [{
            xtype: 'toolbar',
            items: [{
                text: 'Add',
                handler: function () {
                    // empty record
                    var rec = new User();
                    var rowEditing = grid.findPlugin('rowediting');
                    store.add(rec);
                    rowEditing.startEdit(rec, 0);
                }
            }, '-', {
                itemId: 'delete',
                text: 'Delete',
                disabled: true,
                handler: function () {
                    var selection = grid.getView().getSelectionModel().getSelection()[0];
                    if (selection) {
                        store.remove(selection);
                    }
                }
            }, '-', {
                itemId: 'refresh',
                text: 'Refresh',
                handler: function () {
                    // remember current row
                    currentRow = grid.getSelectionModel().getSelection()[0].data.id;
                    store.load();
                }
            }

            ]
        }]
    });
    grid.getSelectionModel().on('selectionchange', function (selModel, selections) {
        grid.down('#delete').setDisabled(selections.length === 0);
    });
});